# myrcc-api-sample

This is an example showing the usage of the myRCC API:
* a login as a "Service Provider" or "Enterprise" Administrator
* Choosing an enterprise to monitor
* Listing the extensions having thr myRCC option activated
* this script was written to help developer understand the use of myRCC API

# Pre-requisites
* Use python3 (3.8 recommended)
* Import the following modules: requests, json, base64, re, socket, websocket, random, time, sys, datetime

# Quick start

Clone the repository:

    git clone git@gitlab.com:centile-pub/myrcc-api-sample

# Launch the script

    python3.8 myrcc_monitor_calls.py

# More info

Please refer to our PartnerConnect website (https://partnerconnect.centile.com) or contact me.
