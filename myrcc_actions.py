# Importing some useful libraries

import requests, json, base64, re, socket, websockets, asyncio, time, logging, pprint, webbrowser, sys
from datetime import datetime

url = "https://myrcc.<your-domain-com>/restletrouter"
message = "<admin_login>|<enterprise_name>:<admin_pass>"


# Encoding base64 the login/password

message_bytes = message.encode()
base64_bytes = base64.b64encode(message_bytes)
base64_message = base64_bytes.decode()
type(base64_message)

# Defining the initial header

payload = {
    'X-Application': 'myRCC',
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*'}
payload['Authorization'] = "Basic " + base64_message

# Login phase

login = requests.post(url + "/v1/service/Login/", headers=payload)
login_ans = str(login.status_code)
if login_ans == "401":
    print("====================================")
    print(f"Check your credentials (Error {login_ans})")
    print("====================================")
    sys.exit()

r_dict = login.headers

# Getting X-Application and Set-Cookie headers, beautifying them (useful later)

xapp = r_dict['X-Application']
cookie = r_dict['Set-Cookie'].split('myRCC_SESSIONID')[-1].split(';')[0]
cookie = "myRCC_SESSIONID" + cookie
#print(cookie)
print("===================")
print("Your Set-Cookie is: " + cookie)
print("Your X-Application is: " + xapp)
print("===================")

# Including the 2 headers to the "authenticated" header

payload_auth = {
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*'}
payload_auth['X-Application'] = xapp
payload_auth['Cookie'] = cookie
#print(payload_auth)

# Getting the list of myRCC extension

extensions = requests.get(url + "/v1/rcc/Extension", headers=payload_auth)
ext_data = extensions.json()
#print(ext_data)

for x in range(0,ext_data['count']):
    print(f"{x+1}: {ext_data['items'][x]['displayName']} - {ext_data['items'][x]['addressNumber']}")


# Display number of myRCC capable extensions in the enterprise
# print("Number of myRCC extensions: " + str(ext_data['count']))

# Fetching details of a specific extension
print("\n")
rep = input(f"Enter the extension number to detail (from 1 to {ext_data['count']}): ")

length = len(ext_data['items'])
x = int(rep)
print("Name: " + str(ext_data['items'][x-1]['displayName']))
print("Extension: " + str(ext_data['items'][x-1]['addressNumber']))
print("Rest URI: " + str(ext_data['items'][x-1]['restUri']))
restUri = ext_data['items'][x-1]['restUri']
print("PSTN: " + str(ext_data['items'][x-1]['pstnNumbers']))
print("Status: " + str(ext_data['items'][x-1]['telephonicState']))

# Creating the WebSocket

url_ws = "wss://myrcc.<your-domain-com>/restletrouter"

async def myrcc_socket():

    async with websockets.connect(url_ws + "/ws-service/myRCC", extra_headers=payload_auth) as websocket:
        
        events = url + "/v1/service/EventListener/bean"
        
        # Put listener on Call Lines

        cl_post = requests.post(events, headers=payload_auth, json={'name':'CallLines'})
        callLine_get = requests.get(url + "/v1/rcc/CallLine?listenerName=CallLines", headers=payload_auth)
        callLine_get_json = json.loads(callLine_get.text)
        
        t1 = datetime.now()
        
        while (datetime.now()-t1).seconds <= 18000:
            rcv = await websocket.recv()
            rcv_json = json.loads(rcv)

            if ("listenerName" in rcv_json):
                pprint.pprint(rcv_json)
                print("===============================")

                # Call in talking state

                if "item" in rcv_json and str(rcv_json['item'][-1]['state']) == "talking":
                    for x in range(0,ext_data['count']):
                        if ext_data['items'][x]['restUri'] == rcv_json['item'][-1]['extension']:
                            user_disp = ext_data['items'][x]['displayName']
                            print(f" - Call Established between {user_disp} and {rcv_json['item'][-1]['displayedNumber']}" + " - " + "\n")
                    
                    
                    # Opening CRM system and search for PSTN number
                    print(" - Opening CRM system - " + "\n")
                    webbrowser.open('https://contacts.google.com/search/' + rcv_json['item'][-1]['displayedExternalNumber'])
                    
                    print("Available actions:")
                    for x in range(0, len(rcv_json['item'][-1]['actions'])):
                        print(f"{x+1}" + " : " + str(rcv_json['item'][-1]['actions'][x]))
                    print("\n")
                    rep = input("Please type your choice: ")

                    # Hold Call
                    if rep == "3":
                        hold_call = requests.get(url + "/" + rcv_json['item'][-1]['restUri'] + "/hold", headers=payload_auth)
                        print(hold_call.status_code)

                    # Disconnect Call
                    if rep == "2":
                        disc_call = requests.get(url + "/" + rcv_json['item'][-1]['restUri'] + "/disconnect", headers=payload_auth)

                    # Redirect Call
                    if rep == "5":
                        dest = input("Please type the destination of the transfer: ")
                        redir_call = requests.post(url + "/" + rcv_json['item'][-1]['restUri'] + "/redirect", headers=payload_auth, json={'destination':dest})

                    # Start / Stop Recording
                    if rep == "8" and str(rcv_json['item'][-1]['isRecording']) == "False":
                        rec_call = requests.get(url + "/" + rcv_json['item'][-1]['restUri'] + "/startRecord", headers=payload_auth)
                    elif rep == "8" and str(rcv_json['item'][-1]['isRecording']) == "True":
                        rec_call = requests.get(url + "/" + rcv_json['item'][-1]['restUri'] + "/stopRecord", headers=payload_auth)
                
                    # Adding Note
                    if rep == "9":
                        time.sleep(2)
                        note = input("Please type your comment on this call: ")
                        add_note = requests.post(url + "/" + rcv_json['item'][-1]['restUri'] + "/addNote", headers=payload_auth, json={'note':note})
                        

                # Call in held state

                if "item" in rcv_json and str(rcv_json['item'][-1]['state']) == "held":
                    print(" - Call Held - with " + str(rcv_json['item'][-1]['displayedNumber']) + " - " + "\n")
                    print("Available actions:")
                    for x in range(0, len(rcv_json['item'][-1]['actions'])): 
                        print(f"{x+1}" + " : " + str(rcv_json['item'][-1]['actions'][x]))
                    rep = input("Please type your choice: ")
                    
                    # Disconnect Call
                    if rep == "1":
                        disc_Call = requests.get(url + "/" + rcv_json['item'][-1]['restUri'] + "/disconnect", headers=payload_auth)

                    # Unhold Call
                    if rep == "2":
                        hold_call = requests.get(url + "/" + rcv_json['item'][-1]['restUri'] + "/unhold", headers=payload_auth)
                    
                    # Redirect the call / One-step tranfer
                    if rep == "4":
                        dest = input("Please type the destination of the transfer: ")
                        redir_call = requests.post(url + "/" + rcv_json['item'][-1]['restUri'] + "/redirect", headers=payload_auth, json={'destination':dest})

                # Call in dropped state

                if "talkingTime" in rcv_json['item'][-1]:
                    call_start = rcv_json['item'][-1]['lastModificationTime']
                    call_end = rcv_json['item'][-1]['talkingTime']
                    call_duration = int((int(call_start)-int(call_end))/1000)
                    call_duration = time.strftime("%H:%M:%S", time.gmtime(call_duration))
                    print(f"    --- Duration: {call_duration}")
                    print("...")
            
            elif callLine_get_json['count'] == 0:
                print("\n")
                print("===============================")
                print("No on going call for the moment")
                print("===============================")
                print("\n")

            # For demo purpose we logout after xxxx seconds

            if (datetime.now()-t1).seconds > 18000:
                logout = requests.get(url + "/v1/service/Logout", headers=payload)
                logout_status = str(logout.status_code)
                print("===============================")
                if logout_status == "204":
                    print(f"Logout OK ({logout_status}) ")
                else:
                    print(f"Error during logout ({logout_status})")
                print("===============================")
                break

asyncio.get_event_loop().run_until_complete(myrcc_socket())