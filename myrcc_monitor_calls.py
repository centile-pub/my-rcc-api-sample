# Importing some useful libraries

import requests, json, base64, re, socket, websockets, asyncio, time, logging, pprint, webbrowser, sys
from datetime import datetime

url = "https://myrcc.<your-domain-com>/restletrouter"
message = "<admin_login>|<enterprise_name>:<admin_pass>"


# Encoding base64 the login/password

message_bytes = message.encode()
base64_bytes = base64.b64encode(message_bytes)
base64_message = base64_bytes.decode()
type(base64_message)

# Defining the initial header

payload = {
    'X-Application': 'myRCC',
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*'}
payload['Authorization'] = "Basic " + base64_message

# Login phase

login = requests.post(url + "/v1/service/Login/", headers=payload)
login_ans = str(login.status_code)
if str(login.status_code) == "401":
    print("====================================")
    print(f"Check your credentials (Error {login.status_code})")
    print("====================================")
    sys.exit()

r_dict = login.headers

# Getting X-Application and Set-Cookie headers, beautifying them (useful later)

xapp = r_dict['X-Application']
cookie = r_dict['Set-Cookie'].split('myRCC_SESSIONID')[-1].split(';')[0]
cookie = "myRCC_SESSIONID" + cookie
print("===================")
print("Your Set-Cookie is: " + cookie)
print("Your X-Application is: " + xapp)
print("===================")

# Including the 2 headers to the "authenticated" header

payload_auth = {
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*'}
payload_auth['X-Application'] = xapp
payload_auth['Cookie'] = cookie

# Getting the list of myRCC extension

extensions = requests.get(url + "/v1/rcc/Extension", headers=payload_auth)
ext_data = extensions.json()

print("The following users are monitored via myRCC: \n")
for x in range(0,ext_data['count']):
    print(f"{x+1}: {ext_data['items'][x]['displayName']} - {ext_data['items'][x]['addressNumber']}")

  
# Creating the WebSocket to receive live events from the server

url_ws = "wss://myrcc.<your-domain-com>/restletrouter"

async def myrcc_socket():

    async with websockets.connect(url_ws + "/ws-service/myRCC", extra_headers=payload_auth) as websocket:
        
        events = url + "/v1/service/EventListener/bean"
        
        # Put listener on Call Lines

        cl_post = requests.post(events, headers=payload_auth, json={'name':'CallLines'})
        callLine_get = requests.get(url + "/v1/rcc/CallLine?listenerName=CallLines", headers=payload_auth)
        callLine_get_json = json.loads(callLine_get.text)
        
        t1 = datetime.now()
        print("===============================")
        print("We are starting to monitor calls...")
        while (datetime.now()-t1).seconds <= 18000:
            rcv = await websocket.recv()
            rcv_json = json.loads(rcv)

            if ("listenerName" in rcv_json):
                
                # Printing the message sent by the server for debugging
                #pprint.pprint(rcv_json)
            
                # Call in ringing state

                if "item" in rcv_json and str(rcv_json['item'][0]['state']) == "ringing":
                    for x in range(0,ext_data['count']):
                        if ext_data['items'][x]['restUri'] == rcv_json['item'][0]['extension']:
                            user_disp = ext_data['items'][x]['displayName']
                            user_ext = ext_data['items'][x]['addressNumber']
                            print(f" - Call Ringing from {user_disp} ({user_ext}) --> {rcv_json['item'][0]['displayedLabel']} ({rcv_json['item'][0]['displayedNumber']})" + " - " + "\n")

                # Call in talking state

                if "item" in rcv_json and str(rcv_json['item'][0]['state']) == "talking":
                    for x in range(0,ext_data['count']):
                        if ext_data['items'][x]['restUri'] == rcv_json['item'][0]['extension']:
                            user_disp = ext_data['items'][x]['displayName']
                            user_ext = ext_data['items'][x]['addressNumber']
                            print(f" - Call Established from  {user_disp} ({user_ext}) --> to {rcv_json['item'][0]['displayedLabel']} ({rcv_json['item'][0]['displayedNumber']})"  + " - " + "\n")
                    

                # Call in dropped state (not all cases are handled here)

                if "item" in rcv_json and (str(rcv_json['item'][0]['state']) == "dropped" and \
                    str(rcv_json['item'][0]['stateCause'] in rcv_json)):
                    if (str(rcv_json['item'][0]['stateCause']) != "causeCallDeclined" or \
                                str(rcv_json['item'][0]['stateCause']) != "causeCallCompletedElsewhere"):

                        for x in range(0,ext_data['count']):
                            if ext_data['items'][x]['restUri'] == rcv_json['item'][0]['extension']:
                                if "displayName" in ext_data['items'][x]:
                                    user_disp = ext_data['items'][x]['displayName']
                                    user_ext = ext_data['items'][x]['addressNumber']
                                    print(f" - Call between {user_disp} ({user_ext}) and {rcv_json['item'][0]['displayedLabel']} ({rcv_json['item'][0]['displayedNumber']})" + " terminated - " + "\n")
                            
                    # Call failed / rejected / wrong number

                    if str(rcv_json['item'][0]['stateCause']) == "causeReorderTone":
                        print("     -  Call rejected (Cause Reorder)")

                    # Calculating talking time

                    if "talkingTime" in rcv_json['item'][0]:
                        call_start = rcv_json['item'][0]['lastModificationTime']
                        call_end = rcv_json['item'][0]['talkingTime']
                        call_duration = int((int(call_start)-int(call_end))/1000)
                        call_duration = time.strftime("%H:%M:%S", time.gmtime(call_duration))
                        print(f"    --- Duration: {call_duration}")
                        print("...")

            elif callLine_get_json['count'] == 0:
                print("\n")
                print("===============================")
                print("No on going call for the moment")
                print("===============================")
                print("\n")

            # For demo purpose we logout after xxxx seconds

            if (datetime.now()-t1).seconds > 18000:
                logout = requests.get(url + "/v1/service/Logout", headers=payload)
                logout_status = str(logout.status_code)
                print("===============================")
                if logout_status == "204":
                    print(f"Logout OK ({logout_status}) ")
                else:
                    print(f"Error during logout ({logout_status})")
                print("===============================")
                break

asyncio.get_event_loop().run_until_complete(myrcc_socket())